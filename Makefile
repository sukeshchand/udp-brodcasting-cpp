all: client server

client: client.c shared.h shared.c
	gcc client.c shared.c -pedantic -Wall -Wextra -Werror -g -o client

server: server.c shared.h shared.c
	gcc server.c shared.c -pedantic -Wall -Wextra -Werror -g -o server

clean:
	rm -rf client server
